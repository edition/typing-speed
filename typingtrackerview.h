#ifndef TYPINGTRACKERVIEW_H
#define TYPINGTRACKERVIEW_H

#include <QWidget>

class TypingTrackerView : public QWidget
{
  Q_OBJECT
public:
  explicit TypingTrackerView(QWidget *parent = nullptr);
  void setParagraph(const QString value);
public slots:
  void keyPressed(QChar);
signals:
  void paragraphFinished(QString originalText, QString userText);
  void wordDone(bool correct);
protected:
  void paintEvent(QPaintEvent*);
private:
  QString m_inputText;
  QString m_paragraphText;
  QString m_currentWord;
  QString m_currentCompareWord;
  int m_currentCompareWordStartIndex;

};

#endif // TYPINGTRACKERVIEW_H
