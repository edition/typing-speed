#include "mainwindow.h"

#include <QApplication>
#include <QFile>
#include <QMessageBox>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  const QString& STRINGS_FILENAME = "paragraphs.txt";
  if (!QFile::exists("paragraphs.txt")) {
    QMessageBox(QMessageBox::Icon::Critical,
                "Missing paragraphs.txt",
                "No file available to read from").exec();
    return 1;
  }
  QFile stringsFile(STRINGS_FILENAME);
  stringsFile.open(QFile::OpenModeFlag::ReadOnly);
  QString fileContent = stringsFile.readAll();
  fileContent.replace('\n', ' ');
  QVector<QString> stringsList = QString(fileContent)
                                   .split('*', Qt::SkipEmptyParts)
                                   .toVector();
  stringsFile.close();
  MainWindow w;
  w.setParagraphs(&stringsList);
  w.show();
  return a.exec();
}
