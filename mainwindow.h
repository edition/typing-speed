#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "actiondialog.h"
#include "typingtrackerview.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
  void keyPressEvent(QKeyEvent*);
  void setParagraphs(QVector<QString>* value);
  void queueNextParagraph();
  void beginTimedTest(const int durationMinutes);
  void lostFocusEvent(QFocusEvent*);

private:
  struct WPMScore {
    double right, wrong;
  };
  Ui::MainWindow *ui;
  TypingTrackerView *m_ttView;
  ActionDialog *m_actionDialog;
  QVector<QString>* m_strings;
  QVector<WPMScore> m_wpmScores;
  int m_currentParagraphIndex;
  double m_correctWords;
  double m_incorrectWords;
  QTimer *m_secondTimer;
  int m_minsElapsed;
  int m_secsElapsed;
  int m_testDurationSecs;

};
#endif // MAINWINDOW_H
