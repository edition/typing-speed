#ifndef ACTIONDIALOG_H
#define ACTIONDIALOG_H

#include <QDialog>

namespace Ui {
class ActionDialog;
}

class ActionDialog : public QDialog
{
  Q_OBJECT

public:
  explicit ActionDialog(QWidget *parent = nullptr);
  ~ActionDialog();
signals:
  void oneMinuteTestButtonPressed();
  void fiveMinuteTestButtonPressed();
  void viewStatsButtonPressed();
  void quitButtonPressed();

private:
  Ui::ActionDialog *ui;
};

#endif // ACTIONDIALOG_H
