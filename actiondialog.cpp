#include "actiondialog.h"
#include "ui_actiondialog.h"

ActionDialog::ActionDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::ActionDialog)
{
  ui->setupUi(this);
  //connect(ui->buttonOneMinuteTest)
  connect(ui->buttonOneMinuteTest,
          &QCommandLinkButton::clicked,
          this,
          [&]() {
            emit oneMinuteTestButtonPressed();
          });
  connect(ui->buttonFiveMinuteTest,
          &QCommandLinkButton::clicked,
          this,
          [&]() {
            emit fiveMinuteTestButtonPressed();
          });
  connect(ui->buttonViewStats,
          &QCommandLinkButton::clicked,
          this,
          [&]() {
            emit viewStatsButtonPressed();
          });
//  connect(ui->buttonViewStats)
  connect(ui->buttonQuit,
          &QCommandLinkButton::clicked,
          this,
          [&]() {
            emit quitButtonPressed();
          });
}

ActionDialog::~ActionDialog()
{
  delete ui;
}
