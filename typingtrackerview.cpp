#include <QThread>
#include <QDebug>
#include <QKeyEvent>
#include "typingtrackerview.h"
#include <QPainter>

TypingTrackerView::TypingTrackerView(QWidget *parent) : QWidget(parent),
  m_currentCompareWordStartIndex(0)
{
}

void TypingTrackerView::setParagraph(const QString value)
{
  m_paragraphText = value;
  m_inputText = "";
  repaint();
}

void TypingTrackerView::keyPressed(QChar keyChar)
{
  if (m_inputText.size() < m_paragraphText.size()) {
    if (keyChar == ' ') {
      //check previous word)
      if (m_currentWord == m_currentCompareWord) {
        emit wordDone(true);
      } else {
        emit wordDone(false);
      }
      qDebug() << m_currentWord << "  " << m_currentCompareWord;
      m_currentCompareWordStartIndex += m_currentCompareWord.size() + 1;
      m_currentWord = "";
      m_currentCompareWord = "";
    } else {
      m_currentWord += keyChar;
      m_currentCompareWord += m_paragraphText[m_currentWord.size() - 1 + m_currentCompareWordStartIndex];
    }
    m_inputText.append(keyChar);
    if (m_inputText.size() == m_paragraphText.size()) {
      update();
      emit paragraphFinished(m_paragraphText, m_inputText);
    }
  }
  update();
}

void TypingTrackerView::paintEvent(QPaintEvent *evt)
{
  Q_UNUSED(evt)
//  painter.setPen(Qt::yellow);
//  painter.drawText(0, 20, "One");
//  painter.setPen(Qt::red);
//  painter.drawText(QFontMetrics(painter.font()).size(Qt::TextSingleLine, "One ").width(), 20, "Two");
  QPainter painter(this);
  painter.setFont(QFont("sans-serif", 22));
  bool newLine = false;
  int ty, tx;
  tx=0;
  ty = 20;
  for (int i=0; i < m_paragraphText.size(); i++) {
    painter.setPen(Qt::gray);
    if (m_inputText.size() > 0 && i < m_inputText.size()) {
      if (m_inputText[i] == m_paragraphText[i]) {
        painter.setPen(Qt::green);
      } else {
        painter.setPen(Qt::red);
      }
    }
    QChar pc = m_paragraphText.at(i);
    QRect charBoundingRect = QFontMetrics(painter.font()).tightBoundingRect(pc);
//    qDebug() << charBoundingRect.width() << m_paragraphText.at(i);

    if (tx > width() - charBoundingRect.width() * 2) {
      newLine = true;
    }
    if (newLine) {
      ty += charBoundingRect.height() + painter.fontMetrics().lineSpacing();
      tx = 0;
      newLine = false;
    }
    painter.drawText(tx, ty, m_paragraphText.at(i));
    tx += charBoundingRect.width() + painter.fontMetrics().rightBearing(pc);
//    targetRect.setSize(QFontMetrics(painter.font())
//                         .boundingRect(m_paragraphText.at(i)).size());
//    qDebug() << targetRect;
    //targetRect.moveLeft(targetRect.width() % width());'m_paragraphText.at(i)'
//    if (m_paragraphText[i] == '\n') {
//      targetRect.moveTop(LINE_HEIGHT);
//    };

  }
}
