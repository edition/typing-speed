#include <QDebug>
#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QKeyEvent>
#include <QMessageBox>
#include <QRandomGenerator>
#include <QScrollBar>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  m_minsElapsed = m_secsElapsed = 0;
  m_strings = {};
  m_actionDialog = new ActionDialog(this);
  m_ttView = new TypingTrackerView(this);
  m_currentParagraphIndex = 0;
  ui->verticalLayout->addWidget(m_ttView);
  setWindowTitle("Typing Speed Tool");
  connect(m_actionDialog, &ActionDialog::oneMinuteTestButtonPressed, this, [&] {
    m_actionDialog->accept();
    beginTimedTest(1);
  });
  connect(m_actionDialog, &ActionDialog::quitButtonPressed, this,
          [&] { close(); });
  connect(m_ttView, &TypingTrackerView::paragraphFinished, this,
          [&](QString originalText, QString userText) {
            QStringList originalTextWords = originalText.split(' ');
            QStringList userTextWords = userText.split(' ');
            for (int i = 0; i < originalTextWords.size(); i++) {
              if (i < userTextWords.size() &&
                  originalTextWords[i] == userTextWords[i]) {
                m_correctWords++;
              } else {
                m_incorrectWords++;
              }
            }
            queueNextParagraph();
          });
  connect(m_ttView, &TypingTrackerView::wordDone, this,
          [&](bool correct) {
            if (correct) {
              qDebug() << "correct word";
              m_correctWords++;
            } else {
              qDebug() << "incorrect word";
              m_incorrectWords++;
            }
            this->setWindowTitle(QString("Accuracy: %1%")
                                   .arg(m_correctWords / (m_correctWords + m_incorrectWords) * 100));
          });
  m_actionDialog->setModal(true);
  m_actionDialog->show();
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::keyPressEvent(QKeyEvent *evt) {
  QChar keyChar = evt->text()[0];
  if (evt->key() == Qt::Key::Key_Enter || evt->key() == Qt::Key::Key_Return) {
    keyChar = '\n';
    ui->labelInput->insertPlainText("\u23ce\n");
  } else if (keyChar.isPrint()) {
    ui->labelInput->insertPlainText(keyChar);
    m_ttView->keyPressed(keyChar);
  }
  ui->labelInput->moveCursor(QTextCursor::End);
//  ui->scrollArea->verticalScrollBar()->setSliderPosition(
//      ui->labelInput->height());
  // evt->text()
}
void MainWindow::setParagraphs(QVector<QString> *value) { m_strings = value; }

void MainWindow::queueNextParagraph() {
  m_ttView->setParagraph(
    m_strings->at(QRandomGenerator(time(0)).bounded(m_strings->size())));
}

void MainWindow::beginTimedTest(const int durationMinutes) {
  m_secondTimer = new QTimer(this);
  m_testDurationSecs = durationMinutes * 60;
  m_secondTimer->setInterval(1000);
  queueNextParagraph();
  connect(m_secondTimer, &QTimer::timeout, this, [&]() {
    ui->labelTime->setText(
        QString("%1:%2").arg(m_minsElapsed).arg(m_secsElapsed));
    if (m_secsElapsed > 59) {
      // update current wpm score
      m_wpmScores.append({.right=m_correctWords,.wrong=m_incorrectWords});
      m_correctWords = 0;
      m_incorrectWords = 0;
      if (m_minsElapsed * 60 + m_secsElapsed == m_testDurationSecs) {
        m_secondTimer->stop();
        double totalCorrect = 0, totalWrong = 0;
        for (auto &score : m_wpmScores) {
          totalCorrect += score.right;
          totalWrong += score.wrong;
        }
        int finalWPMScore = totalCorrect / m_wpmScores.size();
        QMessageBox(QMessageBox::Icon::Information, "WPM Score",
                    QString("Average of %1 WPM\n%2/%3 Correct")
                      .arg(finalWPMScore)
                      .arg(totalCorrect)
                      .arg(totalCorrect + totalWrong))
            .exec();
      }
      m_minsElapsed++;
      m_secsElapsed = 0;
    } else {
      m_secsElapsed += 1;
    }
  });
  m_secondTimer->start();
}

void MainWindow::lostFocusEvent(QFocusEvent *)
{
  setFocus();
}
