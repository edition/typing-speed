# typing-speed
Typing speed tester built with Qt API's for C++.
Highlights the characters you type in either red or green.
You can change the text data by editing "paragraphs.txt" which is copied
to the build directory by cmake.
Tested on Ubuntu 20.04 LTS.

## Preview
![Preview](preview.png)

## Authors
Benjamin Cottrell

## Requirements
Qt 5.15.2
GCC
CMake 3.5 or newer

## Building
Either use Qt Creator's CMake plugin to do it for you, or alternatively
do an out-of-tree build yourself:
```
mkdir build; cd build
cmake --build -DCMAKE_PREFIX_PATH=<path_to_qt> ..
```
